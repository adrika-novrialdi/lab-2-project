from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index,friend_list,add_friend,delete_friend,validate_npm,model_to_dict
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .models import Friend
from .api_csui_helper.csui_helper import CSUIhelper
import requests
import urllib
# Create your tests here.


class Lab7UnitTest(TestCase):

	def test_lab_7_url_is_exist(self):
		response = Client().get('/lab-7/')
		self.assertEqual(response.status_code, 200)

	def test_lab6_using_index_func(self):
		found = resolve('/lab-7/')
		self.assertEqual(found.func, index)

	def test_lab7_using_right_template(self):
		response = Client().get('/lab-7/')
		self.assertTemplateUsed(response, 'lab_7/lab_7.html')

	def test_paginator(self):
		data = ['a','b','c','d','e','f']
		paginator = Paginator(data, 2)
		page1 = paginator.page(1)
		page2 = paginator.page(2)
		page3 = paginator.page(3)
		self.assertEqual(page1.object_list, list(data[0:2]))
		self.assertEqual(page2.object_list, list(data[2:4]))
		self.assertEqual(page3.object_list, list(data[4:6]))

	def test_get_mahasiswa_url_is_exist(self):
		response = Client().get('/lab-7/get-friend-list/')
		self.assertEqual(response.status_code, 200)




	def test_friend_list_page(self):
		friend = Friend.objects.create(friend_name="Anonymous", npm="123456789")
		response = Client().post('/lab-7/add-friend/', {'name':"Anonymous2", 'npm':"123"})
		self.assertEqual(response.status_code, 200)
		response = Client().get('/lab-7/get-friend-list/')
		self.assertEqual(response.status_code, 200)

	def test_delete_friend(self):
		friend = Friend.objects.create(friend_name="Pikachu", npm="12211221")
		response = Client().post('/lab-7/get-friend-list/delete-friend/' + str(friend.npm) + '/')
		self.assertEqual(response.status_code, 302)
		self.assertNotIn(friend, Friend.objects.all())

	def test_validate_npm(self):
		response = self.client.post('/lab-7/validate-npm/')
		html_response = response.content.decode('utf8')
		self.assertEqual(response.status_code, 200)
		self.assertJSONEqual(html_response, {'is_taken':False})



	def test_auth_param_dict(self):
		csui_helper = CSUIhelper()
		auth_param = csui_helper.instance.get_auth_param_dict()
		self.assertEqual(auth_param['client_id'], csui_helper.instance.get_auth_param_dict()['client_id'])
