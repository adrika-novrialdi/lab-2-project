from django.shortcuts import render

import datetime
# Enter your name here
mhs_name = 'Adrika Novrialdi' # TODO Implement this
birth_date =  datetime.date(1998,12,17)


# Create your views here.
def index(request):
    response = {'name': mhs_name,'age': calculate_age(birth_date.year)}
    return render(request, 'index.html', response)

# Calculate age for getting the current age
def calculate_age(birth_year):
    date_today = calculateToday()
    age = date_today.year - birth_year
    return age

#For getting today's date
def calculateToday():
    import datetime
    return datetime.date.today()
