// Calculator
var print = document.getElementById('print');
var erase = false;

var go = function(x) {
  if (x === 'ac') {
    /* implemetnasi clear all */
    print.value = '';
  } else if (x === 'eval') {
      print.value = Math.round(evil(print.value) * 10000) / 10000;
      erase = true;

  }
  else {
    print.value += x;
  }
};

function evil(fn) {
  return new Function('return ' + fn)();
}
// END


$(document).ready(function() {
    $(".chat-text").keydown(function(k) {
    //cek  yang ditekan adalah Enter ditambah alt atau bukan
    if(k.keyCode === 13 && k.altKey) {
    	k.preventDefault(); //default event tombol enter tidak dilakukan
    	var input = $("textarea").val(); //ambil data di textarea
        $("textarea").val(""); //menghapus text area
        if (input.length>0) {
            $(".msg-insert").append('<p class="msg-send">' + input + '</p>' + '<br>'); //munculkan text di body chat
        }
    }

	});

    $(".chat-button").click(function(){
      var input = $("textarea").val(); //ambil data di textarea
      $("textarea").val(""); //menghapus text area

        if (input.length>1) {
            $(".msg-insert").append('<p class="msg-send">' + input + '</p>' + '<br>'); //munculkan text di body chat
        }
    });

  //inisiasi data tema
  var themes = [{"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
    {"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
    {"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
    {"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
    {"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
    {"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
    {"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
    {"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
    {"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
    {"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
    {"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}
	];
	var selectedTheme = {"Indigo":{"bcgColor":"#3F51B5","fontColor":"#FAFAFA"}};

  //default theme jika web pertama kali dibuka
  if (localStorage.getItem("selectedTheme") === null) {
    localStorage.setItem('selectedTheme', JSON.stringify(selectedTheme));
  }
  //simpan data tema di local storage
  localStorage.setItem('themes', JSON.stringify(themes));

  //menload tema ke select2
	$('.my-select').select2({data: themes});





  //menerapkan selectedTheme yang ada di local storage
  var ygDipilih = JSON.parse(localStorage.getItem('selectedTheme'));
    var bcgColor;
    var fontColor;
	for (key in ygDipilih) {
    	if (ygDipilih.hasOwnProperty(key)) {
            bcgColor = ygDipilih[key].bcgColor;
            fontColor = ygDipilih[key].fontColor;
        }

	}
	//Mengubah theme ketika website dibuka / pengaplikasian selected
	$("body").css({"background-color": bcgColor});
	$("footer").css({"color":fontColor});


  //fungsi tombol apply
	$('.apply-button').on('click', function(){
    var valueTheme = $('.my-select').val();
    var selectedTheme = {};
    //mencari tema yang sesuai dengan id
    for(x in themes){
    	if(x==valueTheme){
    		var bcgColor = themes[x].bcgColor;
    		var fontColor = themes[x].fontColor;
    		var text = themes[x].text;
    		$("body").css({"background-color": bcgColor});
			  $("footer").css({"color":fontColor});
    		selectedTheme[text] = {"bcgColor":bcgColor,"fontColor":fontColor};
    		localStorage.setItem('selectedTheme', JSON.stringify(selectedTheme));
    	}
    }
});


});

// END